
![logo](assets/gitlab.png)

### [Gitlab](https://gitlab.com)

+++

### Objetivos

<ol>
  <li>__Asegurar__ la colaboración entre todos los miembros de la unión</li>
  <li class="fragment">__Proveer__ una herramienta eficiente para gestionar el ciclo de vida del desarrollo de software</li>
  <li class="fragment">__Garantizar__ la integración de todos nuestros servicios, siempre que sea posible</li>
</ol>

[https://about.gitlab.com/strategy](https://about.gitlab.com/strategy)

+++

### Características

![bg](assets/gitlab-features.png)

+++

### Características

<ul>
  <li>_Hosting_ para páginas estáticas</li>
  <li class="fragment">Su propio sistema de integración continua y despliegue a producción</li>
  <li class="fragment">Registro para gestionar imágenes de [Docker](https://www.docker.com/)</li>
</ul>

+++

### Características

<ul>
  <li>Resolución de conflictos _online_</li>
  <li class="fragment">Todo lo bueno de [Git](https://git-scm.com/book/es/v1) y [Github](https://github.com/) (pero _offline_)</li>
</ul>

---

### <span style="color: red">K</span>B [Kanboard](https://kanboard.net/)

+++

### Objetivos

<ol>
  <li>__Manejar__ los proyectos forma eficiente y simple</li>
  <li class="fragment">__Mantener__ el enfoque ágil y dar soporte _online_ para metodologías [Scrum](https://es.wikipedia.org/wiki/Scrum_(desarrollo_de_software) y [Kanban](https://es.wikipedia.org/wiki/Kanban_(desarrollo)</li>
  <li class="fragment">__Garantizar__ la integración de todos nuestros servicios, siempre que sea posible</li>
</ol>

[https://about.gitlab.com/applications/#scrum-boards](https://about.gitlab.com/applications/#scrum-boards) <!-- .element: class="fragment" -->

+++

### Características
 
<ul>
  <li>Visualizar el trabajo</li>
  <li class="fragment">__Limita__ tu trabajo en progreso para mejorar la eficiencia</li>
  <li class="fragment">Personaliza tus tableros de acuerdo a tu __modelo de negocio__</li>
</ul> 

+++

![image](assets/kanboard-board.png)

+++

### Características
 
<ul>
  <li>Permite __arrastrar/soltar__ tareas</li>
  <li class="fragment">__Reportes__ y __análisis__ de tus proyectos/progreso</li>
  <li class="fragment">Instalación de __plugins__ para integración con _software_ de terceros</li>
</ul>

+++

### <span style="color: red">LIBRE</span>, _Open Source_, _self-hosted_

---

![logo](assets/jenkins.png)

### [Jenkins](https://jenkins.io/)

+++

### Objetivos

<ol>
  <li>__Ofrecer__ <span style="color: red">otra</span> herramienta completamente dedicada para la integración contínua</li>
  <li class="fragment">__Mantener__ nuestra infraestructura automatizada</li>
  <li class="fragment">__Integración__</li>
</ol>

[http://doc.gitlab.com/ee/integration/jenkins.html](http://doc.gitlab.com/ee/integration/jenkins.html) <!-- .element: class="fragment" -->

+++

### Objetivos

> Hacer fácil, hacerlo bien. <br>
_Martin Fowler_

[https://martinfowler.com/articles/microservices.html](https://martinfowler.com/articles/microservices.html)

+++

### Características
 
<ul>
  <li>__Fácil__ instalación</li>
  <li class="fragment">__Fácil__ configuración</li>
  <li class="fragment">__Escalable__</li>
</ul>

---

![logo](assets/nexus.png)

### [Nexus](https://www.sonatype.com/nexus-repository-sonatype)

+++

### Objetivos

<ol>
  <li>__Incrementar__ velocidad de compilación y mantenerla __reproducible__</li>
  <li class="fragment">__Manejar__ el ciclo de vida de los artefactos</li>
  <li class="fragment">__Ofrecer__ acceso, sin necesidad de <span style="color: red">_internet_</span>, a binarios remotos</li>
</ol>

[https://www.jfrog.com/binary-repository/](https://www.jfrog.com/binary-repository/)

+++

### Características
 
<ul>
  <li>__Soporte__ para todo tipo de componentes</li>
  <li class="fragment">__Despliegue__ de binarios de manera automatizada y controlada</li>
  <li class="fragment">__Búsqueda__ de componentes</li>
</ul>

+++

### Componentes

![Firewall](assets/nexus-firewall.png)
<div>[Nexus Firewall](https://cta-service-cms2.hubspot.com/ctas/v2/public/cs/c/?cta_guid=48bb512f-9d90-4c6a-b25a-862bebed5f1f&placement_guid=0474e07d-4923-475b-8248-d909b53db436&portal_id=1958393&redirect_url=APefjpEyGFuhKPQVi4sks2utY9U9l7xUMhzsfVXqbNyAc6svKfkuU46MQPkzY2HsoDe33DxN03qEcWFTi_tdAXsbV5q6FRem5fV6iZNlU7OiJ_AIJbMFlL86T38lV_RSmR6pNvnyl-OXedY1ERR-RVDF1_VV_pSLf5zf77Wk7-rsr5hvEPG97oGaN6OP0lrIyynlPcsifIJS9fEzTr8rFRpcPLp2Zre7tiX_JzBD99JHzCEokOUKXiRv9ifc_ovvOnv_kGXcPDaOLYzaE86I2vo28SM6iOryLQ&hsutk=cf01d1c80060d6fd7792811bbbe055cb&canon=https%3A%2F%2Fwww.sonatype.com%2Fproducts-overview&click=4ebcd915-8e12-4d63-9895-b55bda1ea9d7&utm_referrer=https%3A%2F%2Fwww.sonatype.com%2Fnexus-repository-sonatype&pageId=4642678694&__hstc=31049440.cf01d1c80060d6fd7792811bbbe055cb.1500313238682.1500384291069.1500389972433.3&__hssc=31049440.1.1500389972433&__hsfp=3446295513)</div>

+++

### Componentes

![Repository](assets/nexus-repository.png)
<div>[Nexus Repository](https://cta-service-cms2.hubspot.com/ctas/v2/public/cs/c/?cta_guid=f2fa2d3c-5d48-48c4-8c63-1835f9628f2b&placement_guid=4f8936c3-d4eb-43b2-97dd-da266dd375ed&portal_id=1958393&redirect_url=APefjpHBe892-3ecHnk4X4FMYUwQKt075Djod9zUawi3UTnk994v_dHdonPNIQSMH4bb9bs4x-tSKmjHIcSJB53h9WSUx5H8oXHpQQTZK12gFsQgNcepmD1yg3JbWsGCrZ8DsQnMCfDZLRAvKgvgemVsDIv-3XcwHh8xCBz4eHX4chk3pGySKi4bYyGIpxpl6kfySJ2L0X0CUhpL40EStzARWC1c_FhgpO-sn5_CJyYoy93rlziCpBRhUgzMwDqwYVY_tXElZiUW16NC4HUrlMkp0CaU6C34OQ&hsutk=cf01d1c80060d6fd7792811bbbe055cb&canon=https%3A%2F%2Fwww.sonatype.com%2Fproducts-overview&click=dc687514-544e-4715-8c62-abeb8ee2d293&utm_referrer=https%3A%2F%2Fwww.sonatype.com%2Fnexus-repository-sonatype&pageId=4642678694&__hstc=31049440.cf01d1c80060d6fd7792811bbbe055cb.1500313238682.1500384291069.1500389972433.3&__hssc=31049440.1.1500389972433&__hsfp=3446295513)</div>

+++

### Componentes

![Lifecycle](assets/nexus-lifecycle.png)
<div>[Nexus Lifecycle](https://cta-service-cms2.hubspot.com/ctas/v2/public/cs/c/?cta_guid=70b57324-ff74-40ea-9309-e3014e82b596&placement_guid=9237f8f0-b0bf-49b4-8245-ec6311dc58ba&portal_id=1958393&redirect_url=APefjpGOqPYNe9CSWidMimYRFltKbblbJoyPOnvSJJUSDCH5krKlmI7NTwPE-UAAca5bDrumK17v4KcLh0FCOzY2uA7Pfj0TtpNVK2mEFfHSF0wE0TxtvIgI26IyA5lQgf-EAUTegmoatxMe9-oDgqHRVGk0eDUa5cpWXoeAungCWC6RZMeRR0djPgjB8Xjat_Mu6brMu-wGgtpCqH8C4jcTnkY7nen14oB5lhXPBMvgylbh0mJmEgsvTKsl4tlWqpi1UCTMZqBV26htHa5Gc-c8O7Z_mKh56da9bHW4PYNGaVxOP-AJJcI&hsutk=cf01d1c80060d6fd7792811bbbe055cb&canon=https%3A%2F%2Fwww.sonatype.com%2Fproducts-overview&click=a4e3bcbd-44ad-4cba-9b63-7ace5f3a98eb&utm_referrer=https%3A%2F%2Fwww.sonatype.com%2Fnexus-repository-sonatype&pageId=4642678694&__hstc=31049440.cf01d1c80060d6fd7792811bbbe055cb.1500313238682.1500384291069.1500389972433.3&__hssc=31049440.1.1500389972433&__hsfp=3446295513)</div>

+++

![Nexus Promo on Vimeo](https://player.vimeo.com/video/217179090)

---

![logo](assets/elasticsearch.png)

### [Elasticsearch](https://www.elastic.co/)

+++

> __Elasticsearch__ es distribuido y proporciona fácil acceso para búsquedas y análisis de datos por __RESTful__. Como el corazón del _stack_, almacena sus datos, así que puedes __encontrar__ lo esperado y __revelar__ lo inesperado.  

[Elastic Team](https://www.elastic.co/products/elasticsearch)

+++

### Características
 
<ul>
  <li>__Reciliencia__ y alta __disponibilidad__</li>
  <li class="fragment">__Fácil__ de integrar y __extender__</li>
  <li class="fragment">__Big Data__ (_Hadoop_ & _Spark_)</li>
</ul>


+++

### <span style="color: red">LIBRE</span>, _Open Source_

---

![logo](assets/postgresql.png)

### [Postgresql](https://www.postgresql.org/)
